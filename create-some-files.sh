#!/bin/sh
if [ ! -d /data/testdir ]; then
    echo "Creating some files"
    mkdir /data/testdir
    cd /data/testdir
    dd if=/dev/zero of=/data/basefile bs=1 count=1000000
    split -b 1 -a 6 /data/basefile
fi

while true; do
    echo "Sleeping ..."
    sleep 10
done
