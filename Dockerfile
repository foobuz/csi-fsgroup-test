FROM alpine
VOLUME /data
ADD create-some-files.sh /usr/bin/
CMD ["/usr/bin/create-some-files.sh"]

