# k8s CSI fsGroup Error Demo

This is just a quick demonstration for
https://github.com/kubernetes/enhancements/blob/master/keps/sig-storage/20200120-skip-permission-change.md

```
kubectl apply -f statefulset.yaml
```
